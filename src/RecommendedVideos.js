import React from 'react';
import './RecommendedVideos.css';
import VideoCard from './VideoCard';

function RecommendedVideos () {
  return (
    <div className="recommendedVideos">
      <div className="recommendedVideos__videos">
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
        <VideoCard 
          title="Youtube Video | "
          views="1M Views"
          timestamp="2 days ago"
          channelImage="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
          channel="John Doe"
          image="https://www.datocms-assets.com/21211/1602248694-github-img.jpg"
        />
      </div>
    </div>
  )
}

export default RecommendedVideos;