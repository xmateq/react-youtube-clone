import React from 'react';
import './SearchPage.css';
import ChannelRow from './ChannelRow';
import VideoRow from './VideoRow';
import TuneOutlinedIcon from '@material-ui/icons/TuneOutlined';

function SearchPage () {
  return (
    <div className="searchPage">
      <div className="searchPage__filter">
        <TuneOutlinedIcon />
        <h2>FILTER</h2>
      </div>
      <hr/>
      <ChannelRow 
        image="https://www.filepicker.io/api/file/9GCYH9YQnyqu4Ymwmzu9"
        channel="John Doe"
        verified
        subs="900K"
        numberOfVideos={438}
        description="Clever Programmer, programming tutorials. blahblahblah blahblahblah blahblahblahblahblahblah blahblahblah blahblahblahblahblahblahblahblahblah blahblahblah"
      />
      <hr />

      <VideoRow 
        views="1.4M"
        subs="900K"
        description="blahblahblah blahblahblah blahblahblahblahblahblah"
        timestamp="1 day ago"
        channel="John Doe"
        title="testTitle"
        image="https://www.wyzowl.com/wp-content/uploads/2019/09/YouTube-thumbnail-size-guide-best-practices-top-examples.png"
      />
      <VideoRow 
        views="1.4M"
        subs="900K"
        description="blahblahblah blahblahblah blahblahblahblahblahblah"
        timestamp="1 day ago"
        channel="John Doe"
        title="testTitle"
        image="https://d12swbtw719y4s.cloudfront.net/images/McVytTcr/jLaLSTpQo7vd4IwJvmx9/VPI1QvL6Zy.jpeg"
      />
      <VideoRow 
        views="1.4M"
        subs="900K"
        description="blahblahblah blahblahblah blahblahblahblahblahblah"
        timestamp="1 day ago"
        channel="John Doe"
        title="testTitle"
        image="https://i.pinimg.com/originals/99/90/11/999011e13645a5b825b76a47877a0a8a.jpg"
      />
      <VideoRow 
        views="1.4M"
        subs="900K"
        description="blahblahblah blahblahblah blahblahblahblahblahblah"
        timestamp="1 day ago"
        channel="John Doe"
        title="testTitle"
        image="https://i.ytimg.com/vi/gtv2vF7jHn4/maxresdefault.jpg"
      />
      <VideoRow 
        views="1.4M"
        subs="900K"
        description="blahblahblah blahblahblah blahblahblahblahblahblah"
        timestamp="1 day ago"
        channel="John Doe"
        title="testTitle"
        image="https://i.pinimg.com/originals/e9/ca/00/e9ca006113ce6d9bc242d08a4d9ffd50.jpg"
      />
    </div>
  )
}

export default SearchPage;